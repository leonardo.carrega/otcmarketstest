import {Selector, t} from 'testcafe';

export interface companyObject{
    name: string,
    ticker: string,
}


class OTCWebSite {
    private ticker: string
    private open: number
    private marketCap: number
    private companyName: string

    private readonly searchBar: Selector = Selector('input').withAttribute('placeholder', 'Quote');
    private readonly overviewTab: Selector = Selector('a').withAttribute('href', '/stock/RHHBY/overview');
    private readonly quoteTab: Selector = Selector('a').withExactText('Quote');
    private readonly quoteOpenValue: Selector = Selector('label').nextSibling()
    private readonly quoteMarketCapValue: Selector = Selector('span').withText('MARKET CAP').nextSibling()
    private readonly securityDetailsTab: Selector = Selector('a').withExactText('Security Details');
    private readonly securityDetailsMarketCapValue: Selector = Selector('b').withText('Market Cap').parent().nextSibling();


    public async searchFor(ticker: string): Promise<void> {
        this.ticker = ticker;
        console.log('Testing ticker', this.ticker);
        await t.typeText(this.searchBar, ticker);
        await t.pressKey('Enter');
    }

    public async clickOnQuoteTab(): Promise<void> {
        await t.click(this.quoteTab);
    }

    public async getOpenAndMarketCapValues(): Promise<void> {
        try{
        const open = await this.quoteOpenValue.innerText;
        // I erase the ',' in the numbers to avoid issues, '.' works ok.
        this.open = parseFloat(open.replace(/,/g, ""))
        console.log('Open Value is: ', this.open);
        const marketCap = await this.quoteMarketCapValue.innerText;
        this.marketCap = parseFloat(marketCap.replace(/,/g, ""))
        console.log('Market Cap Value is: ', this.marketCap);
    }catch{
        throw 'Cannot find values for this tickets'
        }
    }

    public async checkThatCompanyNameOfTicker(companyName: string): Promise<void> {
        this.companyName = await Selector('div').child('h1').withExactText(this.ticker).sibling().innerText;
        console.log('Company name: ', this.companyName);
        await t.expect(this.companyName).eql(companyName);
    }

    public async checkThatCompanyLogoBe(ticker: string): Promise<void> {
        //console.log('Company logo: ', await this.companySymbol().getAttribute('src'))
        try{
            await t.click(this.overviewTab);
            const logo = await Selector('img').withAttribute('alt', `${this.companyName} Company Logo`).getAttribute('src');
            console.log('logo: ', logo);
        }
        catch {
            console.log('This company has no image');
        }
        // finally {
        //     continue;
        // }
        //        await t.expect(this.companySymbol.getAttribute('src')).eql(`https://www.otcmarkets.com/otcapi/company/logo/${ticker}`);
    }

    public async clickOnMarketCap(): Promise<void> {
        await t.click(this.securityDetailsTab);
    }

    public async checkValueMatchInBothTabs(): Promise<void> {
        const marketCapSecurity: string = await this.securityDetailsMarketCapValue().innerText;
        const MCtoDouble: number = parseFloat(marketCapSecurity.replace(/,/g, ""))
        console.log('Market Cap Value inside Security Details: ', MCtoDouble);
        await t.expect(this.marketCap).eql(MCtoDouble);
        console.log(`Market Cap ${this.marketCap.toString()} on ${await this.securityDetailsMarketCapValue().nextSibling().innerText}`);
    }

    public async checkTheFollowing(companies: companyObject[]): Promise<void> {
        for (let company of companies) {
            await t.navigateTo('https://www.otcmarkets.com/')
            await this.searchFor(company.ticker)
            await this.clickOnQuoteTab();
            await this.getOpenAndMarketCapValues();
            await this.checkThatCompanyNameOfTicker(company.name);
            await this.checkThatCompanyLogoBe(company.ticker);
            await this.clickOnMarketCap();
            await this.checkValueMatchInBothTabs();
        }
    }
}
export default new OTCWebSite();