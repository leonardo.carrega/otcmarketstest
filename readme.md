# Automation testing for OTC Markets

This automation was made using Testcafe, you can visit https://testcafe.io  for more info.

## How to run it locally
### Install dependencies
```text
npm install
```
or...
```text
npm install -g testcafe
```

### Run testing scripts
#### Multi Browser
The main script is made to run in 'Chrome', 'Firefox' and emulating an 'Iphone X' at the same time
```text
npm run test
```
or simply
```text
testcafe
```

#### Single browser
if you want to run the tests only in Chrome you can use
```text
npm run test:chrome
```
or a simple 
```text
testcafe chrome
```

Regards!
