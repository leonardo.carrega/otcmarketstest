import pom, {companyObject} from '../pages/POM';

fixture('Demo testing for OTC Markets')
    .page('https://www.otcmarkets.com/');

test('Check values for one ticket', async () => {
    await pom.searchFor('OTCM')
    await pom.clickOnQuoteTab();
    await pom.getOpenAndMarketCapValues();
    await pom.checkThatCompanyNameOfTicker('OTC Markets Group Inc.');
    await pom.checkThatCompanyLogoBe('/logos/tier/QX.png');
    await pom.clickOnMarketCap();
    await pom.checkValueMatchInBothTabs();
})

test('Check values for multiple tickets', async () => {
    let companies: companyObject[] = [
        { ticker: 'RHHBY', name: 'Roche Holding Ltd' },
        { ticker: 'ADBCF' , name: 'ADBRI Limited' }

    ];
    await pom.checkTheFollowing(companies);

})





